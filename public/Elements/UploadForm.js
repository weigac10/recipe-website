class UploadForm extends AbstractCustomElement {

    constructor () {
        super();
    }
            
    connectedCallback () {
        super.connectedCallback();

        // If you ever visit an edit/upload page then you can edit from anywhere
        setShowEditCookie();

        DBMessageService.getSections().then(function (data) {
            var sections = JSON.parse(data).data;
            var selectElement = document.getElementById("sectionInput");
            sections.forEach(function (section) {
                var option = document.createElement("option");
                option.value = section;
                option.innerHTML = section;
                selectElement.appendChild(option);
            });
        });
    }
    
    _getTemplate() {
        return `
            <form id="formWrapper" method="post" enctype="multipart/form-data" name="uploadForm" action="/uploadRecipe" onsubmit="return validateUploadForm()" method="post">
                <div class="form-group">
                    <label for="nameInput" class="col-form-label">Recipe Name</label>
                    <input type="text" class="form-control" id="nameInput" onkeydown="return event.key != 'Enter';" name="name" placeholder="Recipe" required>
                </div>
                
                <div class="form-group">
                    <label for="sectionInput" class="col-form-label">Section</label>
                    <select class="form-control" id="sectionInput" name="section">
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="servingsInput" class="col-form-label">Servings</label>
                    <input type="number" class="form-control" id="servingsInput" onkeydown="return event.key != 'Enter';" name="servings" min="1">
                </div>
                
                <div class="form-group">
                    <label for="timeInput" class="col-form-label">Time</label>
                    <input type="text" class="form-control" id="timeInput" onkeydown="return event.key != 'Enter';" name="time" placeholder="e.g. 1h 30m">
                </div>
                
                <div class="form-group">
                    <label for="imageInput" class="col-form-label">Image</label>
                    <input type="file" class="form-control-file form-control-md" id="imageInput" name="image">
                </div>
                
                <div class="form-group">
                    <label for="ingredientsInput" class="col-form-label">Ingredients</label>
                    <textarea class="form-control" id="ingredientsInput" name="ingredients" rows="10" required></textarea>
                </div>
                
                <div class="form-group">
                    <label for="subIngredients1Name" class="col-form-label">Sub Ingredients 1</label>
                    <input type="text" class="form-control" id="subIngredients1Name" onkeydown="return event.key != 'Enter';" name="subIngredients1Name" placeholder="Name">
                    <textarea class="form-control" id="subIngredients1Input" name="subIngredients1" rows="5"></textarea>
                </div>
                
                <div class="form-group">
                    <label for="subIngredients2Name" class="col-form-label">Sub Ingredients 2</label>
                    <input type="text" class="form-control" id="subIngredients2Name" onkeydown="return event.key != 'Enter';" name="subIngredients2Name" placeholder="Name">
                    <textarea class="form-control" id="subIngredients2Input" name="subIngredients2" rows="5"></textarea>
                </div>
                
                <div class="form-group">
                    <label for="stepsInput" class="col-form-label">Steps</label>
                    <textarea class="form-control" id="stepsInput" name="steps" rows="12" required></textarea>
                </div>
                
                <div class="form-group">
                    <label for="tagsInput" class="col-form-label">Tags</label>
                    <input type="text" class="form-control" id="tagsInput" onkeydown="return event.key != 'Enter';" name="tags" placeholder="e.g. Desserts, Breakfast">
                </div>
                
                <div class="form-group">
                    <label for="uploaderInput" class="col-form-label">Uploader</label>
                    <input type="text" class="form-control" id="uploaderInput" name="uploader" value="Brittany Cormier">
                </div>
                
                <button type="submit" id="submitInput" class="btn btn-primary">Submit</button>
            </form>
        `;
    }
    
};
customElements.define('upload-recipe-form', UploadForm);

    
function validateUploadForm() {
    console.log("Validating");
    var recipeNameDiv = document.getElementById("nameInput");
    return true;
}