class RecipeCardDeck extends AbstractCustomElement {

    constructor () {
        super();

        this._cards = [];
    }

    connectedCallback () {
        super.connectedCallback();

        this._recipes = JSON.parse(this.getAttribute('recipes'));
        this._isPaginated = JSON.parse(this.getAttribute('isPaginated'));

        this._getSearchResults = debounce(this._unDebouncedGetSearchResults.bind(this));

        this._addAllCards();

        // Set banner to random recipe
        this._setupBannerImage();

        this._listenToSearch();
    }

    _addAllCards () {
        // document.getElementById("numRecipesCounter").innerHTML = "<span class=\"badge\">" + this._recipes.length + "</span>";

        this._recipes.forEach(function (recipe, i) {
            var card = this._addCard(recipe);
            this._cards.push(card);

            // Set opacity to fade in from left to right
            card.style.setProperty('--delay', (((i + 1) / this._recipes.length) / 2) + "s");
        }, this);

        setTimeout ( function () {
            document.getElementById("recipeCardDeck").classList.add("loaded");
        }, 300);

        this._setupInfiniteScroll(this._cards);
        this._setupPaginatedLoad();
    }

    _requestAnotherPage () {
        if (this._isPaginated) {
            const promise = DBMessageService.getPaginatedRecipes(this._recipes[this._recipes.length - 1].uploadTime);
            promise.then(recipes => {
                const parsedRecipes = JSON.parse(recipes);

                this._recipes.push(...parsedRecipes);

                const cards = [];
                parsedRecipes.forEach(function (recipe, i) {
                    var card = this._addCard(recipe);
                    cards.push(card);

                    // Set opacity to fade in from left to right
                    card.style.setProperty('--delay', (((i + 1) / this._recipes.length) / 2) + "s");
                }, this);

                this._cards.push(...cards);
                this._setupInfiniteScroll(cards);
                this._setupPaginatedLoad();
            });
        }
    }

    _showSearchedRecipes (recipes) {
        document.getElementById('recipeCardDeck').innerHTML = '';

        recipes.forEach(recipe => {
            const card = this._addCard(recipe);
            card.className += "search-result";
        });
    }

    _setupBannerImage () {
        let div = document.getElementById("bannerImage");

        DBMessageService.getRandomRecipe()
            .then(jsonRecipe => {
                const recipe = JSON.parse(jsonRecipe);
                let imageSrc = recipe.imageLocation;
                if (!imageSrc) {
                    imageSrc = 'https://vaya.in/recipes/wp-content/themes/neptune-by-osetin/assets/img/placeholder.jpg';
                }
                const recipeTitle = recipe.name;
                div.style.backgroundImage = 'url(' + imageSrc + ')';

                const link = document.createElement('a');
                link.href = "/recipe/" + recipe.name.replace(/ /g, "_");
                link.innerHTML = recipe.name;
                document.getElementById('bannerTitle').appendChild(link);
            })

        setTimeout ( () => {
            div.classList.add("loaded");
        }, 300);
    }

    _getTemplate() {
        return `
            <div id="bannerWrapper">
                <div id="bannerImage" />
                <form class="search-wrapper form-inline d-flex justify-content-center md-form form-sm mt-0" onsubmit="return false;">
                    <div class="searchBar-wrapper">
                        <input id="searchBar" autocomplete="off" class="form-control form-control-sm" type="text" placeholder="Search" aria-label="Search">
                    </div>
                </form>
                <div id="bannerTitle"></div>
            </div>

            <div id="numRecipesCounter"></div>
            <div id="recipeCardDeck">
            </div>
        `;
    }

    _addCard (recipe, index) {
        var card = this._createCard(recipe);
        const deck = document.getElementById("recipeCardDeck");

        // Set opacity to fade in from left to right
        card.style.setProperty('--delay', (((index + 1) / this._cards.length) / 2) + "s");
        card.setAttribute('recipe-index', index);

        deck.appendChild(card);
        return card;
    }

    _createCard (recipe) {
        var card = document.createElement("div");
        card.classList = "recipe-card";
        card.style.setProperty('--section-color', SectionUtilities.getColorForSection(recipe.section));

        var imageWrapper = document.createElement("div");
        imageWrapper.classList = "recipe-image-wrapper";
        card.appendChild(imageWrapper);

        var imageDiv = document.createElement("img");
        imageDiv.classList = "recipe-img";
        imageDiv.src = recipe.thumbnail || recipe.imageLocation || "https://www.medicinalgenomics.com/wp-content/uploads/2019/01/image-coming-soon-ecc.png";
        imageWrapper.appendChild(imageDiv);

        var cardBody = document.createElement("div");
        cardBody.classList = "recipe-body";
        card.appendChild(cardBody);

        var sectionDiv = document.createElement("div");
        sectionDiv.classList = "recipe-section";
        sectionDiv.innerHTML = recipe.section;
        cardBody.appendChild(sectionDiv);

        var titleDiv = document.createElement("div");
        titleDiv.classList = "recipe-title";
        titleDiv.innerHTML = recipe.name;
        cardBody.appendChild(titleDiv);

        var age = this._getDaysOld(recipe);
        if (age <= 3) {
            var ageDiv = document.createElement("div");
            ageDiv.classList = "recipe-age badge badge-info";
            ageDiv.innerHTML = "New";
            card.appendChild(ageDiv);
        }

        var link = document.createElement("a");
        link.classList.add('off-screen'); // start everybody off screen
        link.href = "/recipe/" + recipe.name.replace(/ /g, "_");
        link.appendChild(card);

        return link;
    }

    _getDaysOld (recipe) {
        let ms = Date.now() - recipe.uploadTime;
        let seconds = ms / 1000;
        let minutes = seconds / 60;
        let hours = minutes / 60;
        let days = hours / 24;
        return Math.floor(days);
    }

    _listenToSearch () {
        let searchBar = document.getElementById("searchBar");
        searchBar.oninput = () => {
            let searchText = searchBar.value;
            if (searchText) {
                window.scrollTo(0, 0);
                this._getSearchResults(searchText);
                document.getElementById("bannerWrapper").classList.add("searching");
            }
            else {
                document.getElementById("bannerWrapper").classList.remove("searching");
                document.getElementById("recipeCardDeck").innerHTML = '';

                this._cards.forEach(card => {
                    document.getElementById("recipeCardDeck").appendChild(card);
                })

            }
        };
    }

    _unDebouncedGetSearchResults (searchText) {
        DBMessageService.getRecipesFromSearch(searchText)
            .then(jsonRecipes => {
                const recipes = JSON.parse(jsonRecipes);
                this._showSearchedRecipes(recipes);
            });
    }

    _setupInfiniteScroll (cards) {
        var intObserver = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    entry.target.classList.remove('off-screen');
                } else {
                    // Only do this one way since it's strange to scroll back up to see elements reappear
                    // entry.target.classList.add('off-screen');
                }
            })
        },{
            root: null,
            threshold: 0.1
        });
        

        cards.forEach(card => intObserver.observe(card));
    }

    _setupPaginatedLoad () {
        if (!this._isPaginated) {
            return;
        }

        if (this._paginationIntersectionObserver) {
            this._paginationIntersectionObserver.disconnect();
            this._paginationIntersectionObserver = null;
        }

        this._paginationIntersectionObserver = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    this._paginationIntersectionObserver.disconnect();
                    this._paginationIntersectionObserver = null;

                    this._requestAnotherPage();
                } else {
                    // Only do this one way since it's strange to scroll back up to see elements reappear
                    // entry.target.classList.add('off-screen');
                }
            })
        },{
            root: null,
            threshold: 0.1
        });
        
        this._paginationIntersectionObserver.observe(this._cards[this._cards.length - 1]);
    }

};
customElements.define('recipe-card-deck', RecipeCardDeck);