class RecipeGrid extends AbstractCustomElement {

    constructor () {
        super();
    }
            
    connectedCallback () {
        super.connectedCallback();

        this._recipes = JSON.parse(this.getAttribute('recipes'));

        const grid = document.getElementById("recipeGrid");

        this._recipes.forEach(function (recipe, i) {
            const card = this.makeCard(recipe, i);
            if (card) {
                grid.appendChild(card);
            }
        }, this);

        setTimeout ( function () {
            document.getElementById("recipeGrid").classList.add("loaded");
        }, 1000);

    }

    makeCard (recipe, i) {        
        const imgSrc = recipe.thumbnail;
        if (imgSrc) {
            const img = document.createElement('img');
            img.className = 'grid-card-image';
            img.src = imgSrc;
            
            // Set opacity to fade in from left to right
            img.style.setProperty('--delay', (((i + 1) / this._recipes.length) * 10) + "s");

            const title = document.createElement('div');
            title.className = 'grid-card-title';
            title.innerHTML = recipe.name;

            const cardDiv = document.createElement('a');
            cardDiv.className = 'grid-card';
            cardDiv.href = "/recipe/" + recipe.name.replace(/ /g, "_");

            cardDiv.appendChild(img);
            cardDiv.appendChild(title);

            return cardDiv;
        }
        return null;
    }
    
    _getTemplate() {
        return `
            <div id='recipeGrid'>
            </div>
        `;
    }
    
}
customElements.define('recipe-grid', RecipeGrid);