class SearchUtilities {
    
    constructor () {
    
    }
    
    /**
     * Returns indices into allRecipes where the title contains the searchText
     * 
     * @param  {[Object]} allRecipes 
     * @param  {String} searchText 
     * @return {[Integer]} 
     */
    static findRecipesContaining (allRecipes, searchText) {
        var indices = [];
        allRecipes.forEach((recipe, index) => {
            if (recipe.name.toUpperCase().match(searchText.toUpperCase()) !== null) {
                indices.push(index);
            }
        });
        return indices;
    }
};